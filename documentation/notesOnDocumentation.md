# Notes on this Documentation

This documentation was first prepared by Will Pzegeo as part of the *MEEN30170  Introduction to Engineering Research* module at University College Dublin, 2020. The documentation content is based on the training *P. Cardiff, Solid mechanics and fluid-solid interaction using the solids4foam toolbox, 14th OpenFOAM Workshop, Duisburg, Germany, July 2019*, found here: https://www.researchgate.net/publication/335126451_Solid_mechanics_and_fluid-solid_interaction_using_the_solids4foam_toolbox.

The documentation is now maintained by Philip Cardiff. If you find errors or have requests for additional sections, please create an issue on the bitbucket page. If you would like to contribute to it, please create a pull request on the bitbucket page (you will need to contact philip.cardiff@ucd.ie first to request write access to the repository).
